@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('add/movie') }}">
                        @csrf

                        <div class="form-group row">
                           <div class="col">
                                <input id="title" placeholder="Film címe" type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" name="title" required autofocus>
                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0 justify-content-center">
                            <div class="col text-center">
                                <button type="submit" class="btn btn-primary login-button">
                                    {{ __('Hozzáadás') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

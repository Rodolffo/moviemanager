@extends('layouts.app')
@inject('movie', 'App\Movie')
@inject('product', 'App\Product')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @foreach($combinations as $combination)
                    <div class="row">
                        <div  style="text-align: center; font-weight: bold; font-size: 20px" class="col">
                            {{ $movie->find($combination->movie1_id)->title }}
                        </div>  
                        <div style="text-align: center; font-weight: bold; font-size: 20px" class="col">
                            {{ $movie->find($combination->movie2_id)->title }}
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col">
                            @foreach($product->where('movie_id', $combination->movie1_id)->get() as $prod)
                            <div class="row">
                                {{ $prod->name }}
                            </div>
                            @endforeach
                        </div>
                        <div class="col">
                            @foreach($product->where('movie_id', $combination->movie2_id)->get() as $prod)
                            <div class="row">
                                {{ $prod->name }}
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

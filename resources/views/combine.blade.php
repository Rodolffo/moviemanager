@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('combine') }}">
                        @csrf
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <select name="movie1" class="form-control">
                                        @foreach($movies as $movie)
                                        <option value="{{ $movie->id }}">{{ $movie->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <select name="movie2" class="form-control">
                                        @foreach($movies as $movie)
                                        <option value="{{ $movie->id }}">{{ $movie->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px">
                                <div class="col">
                                    <div class="form-group row mb-0 justify-content-center">
                                        <div class="col text-center">
                                            <button type="submit" class="btn btn-primary login-button">
                                                {{ __('Hozzáadás') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

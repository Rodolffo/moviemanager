@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('assign') }}">
                        @csrf

                        <div class="row">
                            <div class="col"></div>
                            @foreach($movies as $movie)
                            <div class="col">{{ $movie->title }}</div>
                            @endforeach
                        </div>
                        @foreach($products as $product)
                        <div class="row">
                            <div class="col">{{ $product->name }}</div>
                            @foreach($movies as $movie)
                            <div class="col"><input type="radio" name="{{ $product->name }}" value="{{ $movie->title }}" 
                            {{ $product->movie_id == $movie->id ? 'checked' : '' }}></div>
                            @endforeach
                        </div>
                        @endforeach
                        <div class="form-group row mb-0 justify-content-center">
                            <div class="col text-center">
                                <button type="submit" class="btn btn-primary login-button">
                                    {{ __('Mentés') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

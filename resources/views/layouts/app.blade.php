<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="main-nav navbar navbar-expand-lg navbar-dark bg-dark">
            @guest
            @else
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item {{ Request::is('add/movie') ? 'active' : '' }}">
                        <a class="nav-link" href="/add/movie">Film hozzáadása</span></a>
                    </li>
                    <li class="nav-item {{ Request::is('add/product') ? 'active' : '' }}">
                        <a class="nav-link" href="/add/product">Termék hozzáadása</a>
                    </li>
                    <li class="nav-item {{ Request::is('assign') ? 'active' : '' }}">
                        <a class="nav-link" href="/assign">Film-termék hozzárendelés</a>
                    </li>
                    <li class="nav-item {{ Request::is('combine') ? 'active' : '' }}">
                        <a class="nav-link" href="/combine">Kombinált műsor készítés</a>
                    </li>
                    <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                        <a class="nav-link" href="/">Műsorok</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/logout">Kijelentkezés</a>
                    </li>
                </ul>
            </div>
            @endguest
        </nav>
            @yield('content')
    </div>
</body>
</html>

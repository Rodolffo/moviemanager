@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('add/product') }}">
                        @csrf

                        <div class="form-group row">
                           <div class="col">
                                <input id="name" placeholder="Név (pl. popcorn)" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                           <div class="col">
                                <input id="qty" placeholder="Mennyiség (pl. 800)" type="text" class="form-control{{ $errors->has('qty') ? ' is-invalid' : '' }}" name="qty" required autofocus>

                                @if ($errors->has('qty'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('qty') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                           <div class="col">
                                <input id="price" placeholder="Ár (pl. 15$)" type="text" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" required autofocus>

                                @if ($errors->has('price'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0 justify-content-center">
                            <div class="col text-center">
                                <button type="submit" class="btn btn-primary login-button">
                                    {{ __('Hozzáadás') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

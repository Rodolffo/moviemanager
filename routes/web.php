<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', '\App\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');
Route::post('login', '\App\Http\Controllers\Auth\LoginController@login');
Route::post('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');
Route::get('add/movie', 'HomeController@addMovie');
Route::post('add/movie', 'HomeController@postMovie')->name('add/movie');
Route::get('add/product', 'HomeController@addProduct');
Route::post('add/product', 'HomeController@postProduct')->name('add/product');
Route::get('assign', 'HomeController@assignProduct');
Route::post('assign', 'HomeController@postAssign')->name('assign');
Route::get('combine', 'HomeController@combineShows');
Route::post('combine', 'HomeController@postCombine')->name('combine');

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Movie;

class Movie extends Model
{
    
    protected $fillable = ['title'];

    public function getProducts() {
        return $this->hasMany('App\Product');
    }

    public function getCombinations() {
        return $this->belongsToMany('App\Movie', 'combinations', 'id', 'movie1_id', 'movie2_id');
    }

}

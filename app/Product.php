<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    protected $fillable = ['name', 'qty', 'price', 'movie_id'];

    public function getMovie() {
        return $this->belongsTo('App\Movie');
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Movie;
use App\Combination;

use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home', ['combinations' => Combination::all()]);
    }

    public function addMovie()
    {
        return view('addmovie');
    }
    public function postMovie(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:movies'
        ]);

        Movie::create([
            'title' => $request->title
        ]);

        return redirect('home');
    }

    public function addProduct()
    {
        return view('additem');
    }
    public function postProduct(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:products',
            'qty' => 'required|numeric',
            'price' => 'required'
        ]);

        Product::create([
            'name' => $request->name,
            'qty' => $request->qty,
            'price' => $request->price
        ]);

        return redirect('home');
    }

    public function combineShows()
    {
        return view('combine', ['movies' => Movie::all()]);
    }
    public function postCombine(Request $request)
    {
        Log::info($request->movie1 . " assigned to " . $request->movie2);
        Combination::create([
            'movie1_id' => Movie::find($request->movie1)->id,
            'movie2_id' => Movie::find($request->movie2)->id
        ]);

        return redirect('home');
    }

    public function assignProduct()
    {
        return view('assign', ['products' => Product::all(), 'movies' => Movie::all()]);
    }
    public function postAssign(Request $request)
    {
        foreach(Product::all() as $product) {
            $movie = Movie::where('title', $request->input(str_replace(" ", "_", $product->name)))->first();
            $product->update(array('movie_id' => $movie->id));
        }
        
        return redirect('home');
    }
}

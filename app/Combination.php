<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Combination extends Model
{
    protected $fillable = ['movie1_id', 'movie2_id'];

    function getFirstMovie() {
        return $this->belongsToMany('App\Movie', 'id', 'movie1_id');
    }
    function getSecondMovie() {
        return $this->belongsToMany('App\Movie', 'id', 'movie2_id');
    }

}
